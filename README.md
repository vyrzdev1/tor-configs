# tor-configs
----------------------------------------------
## An assortment of necessary configs to enable Tor access inside a heavily managed network that employs deep packet inspection.

### Dependencies

- obfs4proxy
- tor
